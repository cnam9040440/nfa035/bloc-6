package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.core.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.DatabaseDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {

    private OutputStream sourceOutputStream;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64DatabaseImpl(OutputStream sourceOutputStream) {
        this.setSourceOutputStream(sourceOutputStream);
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {

        // Récupération de l'instance de lecture séquentielle du fichier de base csv
        BufferedReader br = media.getEncodedImageReader(true);
        // Lecture de la ligne et parsage des différents champs contenus dans la ligne
        String[] data = br.readLine().split(";");
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
           getDeserializingStream(data[2]).transferTo(os);
        }
    }

    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }

    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
