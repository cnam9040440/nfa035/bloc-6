package fr.cnam.foad.nfa035.badges.core.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Interface spécifiant le contrat comportemental du media de sérialisation
 *
 * Cette interface a un rôle de spécialisation de son interface parente,
 * dans le sens où elle doit permettre de maintenir l'état du flux d'entrée afin de favoriser la reprise.
 *
 *
 * @param <T>
 */
public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {

    /**
     * Permet de spécifier lors de la récupération du flux d'entrée,
     * si l'on souhaite reprendre la lecture depuis le denier octet lu
     *
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
    
}
