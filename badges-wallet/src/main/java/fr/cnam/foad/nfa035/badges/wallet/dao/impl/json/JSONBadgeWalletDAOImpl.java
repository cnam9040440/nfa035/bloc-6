package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.core.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.WalletSerializer;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.
 * + Format JSON
 */
@Component("jsonBadge")
public class JSONBadgeWalletDAOImpl implements JSONBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath le chemin vers la base JSON
     */
    public JSONBadgeWalletDAOImpl(@Value("${db.path:badges-wallet/src/test/resources/db_wallet.json}") String dbPath) {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image l'image
     * @throws IOException l'exception
     * @hidden
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence addBadge(DigitalBadge badge)
     */
    @Override
    @Deprecated
    public void addBadge(File image) throws IOException {
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge le badge
     * @throws IOException l'exception
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException {
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer<DigitalBadge, WalletFrameMedia> serializer = new JSONWalletSerializerDAImpl(getWalletMetadata("rw"));
            serializer.serialize(badge, media);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @param badge le badge à supprimer
     * @throws IOException
     */
    @Override
    public void removeBadge(DigitalBadge badge) throws IOException {
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            WalletSerializer serializer = new JSONWalletSerializerDAImpl(getWalletMetadata("rw"));
            serializer.rollback(badge, media);
        }
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet en spécifiant un InputStream spécifique
     * et byPasser le chanp de type File
     *
     * @param badge le badge
     * @throws IOException l'exception
     */
    @Override
    public void addBadge(DigitalBadge badge, InputStream source) throws IOException {
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer<DigitalBadge, WalletFrameMedia> serializer = new JSONWalletSerializerDAImpl(getWalletMetadata("rw")){
                @Override
                public InputStream getSourceInputStream(DigitalBadge ignoreSource) throws IOException {
                    return source;
                }
            };
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream le flux d'écriture de l'image
     * @throws IOException l'exception
     * @deprecated Sur les nouveaux formats de base CSV, Utiliser de préférence getBadgeFromMetadata
     */
    @Override
    @Deprecated
    public void getBadge(OutputStream imageStream) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public DigitalWallet getWalletMetadata() throws IOException {
        return getWalletMetadata("r");
    }

    /**
     * Permet à une autre méthode rw de s'appuyer dessus
     *
     * @return DigitalWallet
     */
    DigitalWallet getWalletMetadata(String mode) throws IOException {
        try (WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, mode))) {
            return getWalletMetadata(mode, media);
        }
    }

    /**
     * Méthode factorisée, permet l'accès depuis l'engine
     * @param mode
     * @param media
     * @return DigitalWallet les metas
     * @throws IOException
     */
    public static DigitalWallet getWalletMetadata(String mode, WalletFrame media) throws IOException {
        Set<DigitalBadge> badges =  new MetadataDeserializerJSONImpl().deserialize(media);
        Set<Long> deadLinesPositions = new HashSet<>();
        DigitalWallet wallet = new DigitalWallet(badges,deadLinesPositions);

        for (Iterator<DigitalBadge> iterator = badges.iterator(); iterator.hasNext();) {
            DigitalBadge badge = iterator.next();
            if (badge.getMetadata().getBadgeId() == -1){
                iterator.remove();
                deadLinesPositions.add(badge.getMetadata().getWalletPosition());
            }
        }
        return wallet;
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        DigitalWallet metas = this.getWalletMetadata();
        try (WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas.getAllBadges()).deserialize(media, meta);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
        DigitalWallet metas = this.getWalletMetadata();
        try (WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas.getAllBadges()).deserialize(media, meta);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @return
     * @throws IOException
     */
    @Override
    public SortedMap<DigitalBadge, DigitalBadgeMetadata> getWalletMetadataMap() throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata("r").getAllBadges();

        // Utilisation de Stream ... en cas de collision, on conserve le 1er élément.
        return metas.stream().collect(
                Collectors.toMap(
                        Function.identity(),
                        DigitalBadge::getMetadata,
                        (a, b) -> a,
                        TreeMap::new
                        ));
    }

    /**
     * {@inheritDoc}
     *
     * @return
     * @throws IOException
     */
    @Override
    public SortedMap<String, DigitalBadge> getWalletMapByHash() throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata("r").getAllBadges();

        // Utilisation de Stream ... en cas de collision, on conserve le 1er élément.
        return metas.stream().collect(
                Collectors.toMap(
                        a -> a.getMD5Hash(),
                        Function.identity(),
                        (a, b) -> a,
                        TreeMap::new
                ));
    }

}