package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.core.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;
import java.text.MessageFormat;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class ImageSerializerBase64DatabaseImpl
        extends AbstractStreamingImageSerializer<File, ResumableImageFrameMedia> {

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws IOException {
        return new FileInputStream(source);
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(ResumableImageFrameMedia media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(),true,0,null));
    }


    /**
     * {@inheritDoc}
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public final void serialize(File source, ResumableImageFrameMedia media) throws IOException {
        File f = (File)media.getChannel();
        long numberOfLines = f.exists() ? Files.lines((f).toPath()).count():0;
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()){
            Writer writer = new PrintWriter(os);
            writer.write(MessageFormat.format("{0,number,#};{1,number,#};", numberOfLines + 1, size));
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                eos.flush();
                writer.write("\n");
            }
            writer.flush();
        }
    }

    /**
     * {@inheritDoc}
     * @param inputStream
     * @param <K>
     * @throws IOException
     */
    @Override
    public <K extends InputStream> void changeSourceInputStream(K inputStream) throws IOException {
        throw new IOException("Non implémenté pour ce type de sérialiseur");
    }
}
