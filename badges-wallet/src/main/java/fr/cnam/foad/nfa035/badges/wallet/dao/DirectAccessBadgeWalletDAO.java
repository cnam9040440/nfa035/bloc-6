package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface définissant le comportement élémentaire d'un DAO destinée à la gestion de badges digitaux
 * PAR ACCES DIRECT, donc nécessitant la prise en compte de métadonnées
 */
public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {

    /**
     * Permet de récupérer les metadonnées du Wallet
     *
     * @return List<DigitalBadgeMetadata>
     * @throws IOException
     */
    DigitalWallet getWalletMetadata() throws IOException;


    /**
     * Permet de récupérer un badge du Wallet à partir de ses métadonnées
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException;

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param badge
     * @throws IOException
     */
    void addBadge(DigitalBadge badge) throws IOException;

    /**
     * Permet de supprimer un badge au Wallet
     *
     * @param badge
     * @throws IOException
     */
    void removeBadge(DigitalBadge badge) throws IOException;
}
