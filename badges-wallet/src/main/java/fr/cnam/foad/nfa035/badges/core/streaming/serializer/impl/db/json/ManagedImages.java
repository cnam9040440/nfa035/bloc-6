package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json;

/**
 * Commentez-moi
 */
public enum ManagedImages {

    jpeg("image/jpeg"), jpg("image/jpeg"), png("image/png"), gif("image/gif");

    private String mimeType;

    /**
     * Commentez-moi
     * @param mimeType
     */
    ManagedImages(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * Commentez-moi
     * @return
     */
    public String getMimeType() {
        return mimeType;
    }
}
