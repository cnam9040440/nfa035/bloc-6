package fr.cnam.foad.nfa035.badges.core.streaming.serializer;

import fr.cnam.foad.nfa035.badges.core.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface définissant le comportement attendu d'un objet servant à la désérialisation d'un badge.
 * Ces objets doivent donc désérialiser à partir d'un média typé,
 * et il doit être possible d'en obtenir un flux de lecture pour l'obtention du contenu désérialisé.
 *
 * @param <M>
 */
public interface BadgeDeserializer<M extends ImageFrameMedia> {

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * Utile pour modifier le flux de sortie au format source
     *
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);


}
