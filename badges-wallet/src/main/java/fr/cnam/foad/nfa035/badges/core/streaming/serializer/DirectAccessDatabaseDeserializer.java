package fr.cnam.foad.nfa035.badges.core.streaming.serializer;

import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface définissant le comportement attendu d'un objet servant  à la désérialisation d'une image.
 * Ces objets doivent donc désérialiser à partir d'un média typé,
 * et il doit être possible d'en obtenir un flux de lecture pour l'obtention du contenu désérialisé.
 *
 * Par ailleurs, ceci est une spécialisation pour un Wallet en accès direct
 *
 */
public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia> {

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */
    void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException;

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * Utile pour modifier le flux de sortie au format source
     *
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);




}
