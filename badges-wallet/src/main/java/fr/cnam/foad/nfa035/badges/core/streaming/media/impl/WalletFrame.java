package fr.cnam.foad.nfa035.badges.core.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.core.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 * @author tvonstebut
 *
 * Implémentation d'ImageFrame pour un Fichier csv comme canal
 * Permet notamment de gérer l'amorce du csv afin de le rendre exploitable par accès direct (RandomAccessFile)
 */
public class WalletFrame extends AbstractImageFrameMedia<RandomAccessFile> implements WalletFrameMedia, AutoCloseable {

    private static final Logger LOG = LogManager.getLogger(WalletFrame.class);

    private BufferedReader encodedImageReader;
    private FileOutputStream encodedImageOutput = null;
    private long numberOfLines;

    /**
     * Constructeur élémentaire
     *
     * @param walletDatabase
     */
    public WalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     *
     * Permet l'initialisation du fichier csv (amorce) si celui-ci est vierge,
     * sinon récupère l'amorce et l'utilise pour initialiser les méta-données
     * globales du média (nombre de lignes...)
     *
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {

        if (encodedImageOutput == null){

            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            if(fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                writer.write(MessageFormat.format("{0,number,#};{1,number,#};", 1, file.getFilePointer()));
                writer.flush();
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                LOG.debug("Dernière ligne du fichier : {}", lastLine);
                String[]data = lastLine.split(";");
                this.numberOfLines = Long.parseLong(data[0]) - 1;
                file.seek(fileLength);
            }

        }
        return encodedImageOutput;
    }

    /**
     * {@inheritDoc}
     *
     * @return InputStream
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new BufferedInputStream(new FileInputStream(getChannel().getFD()));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (encodedImageReader == null || !resume){
            this.encodedImageReader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel().getFD()), StandardCharsets.UTF_8));
        }
        return this.encodedImageReader;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public long getNumberOfLines() {
        return numberOfLines;
    }

    /**
     * Setter protected du nombre de lignes
     * @param numberOfLines
     */
    protected void setNumberOfLines(long numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incrementLines() {
        this.numberOfLines++;
    }


}
