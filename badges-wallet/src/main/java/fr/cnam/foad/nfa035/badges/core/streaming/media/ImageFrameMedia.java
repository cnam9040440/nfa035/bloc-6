package fr.cnam.foad.nfa035.badges.core.streaming.media;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface spécifiant le contrat comportemental du media de sérialisation
 *
 * @param <T>
 */
public interface ImageFrameMedia<T> {

    /**
     * Permet d'obtenir le canal de distribution de notre image sérialisée, potentiellement un Fichier ou bien même un Flux
     *
     * @return le canal
     */
    T getChannel() ;

    /**
     * Permet d'obtenir le flux d'écriture sous-tendant à notre canal
     *
     * @return le flux d'écriture
     * @throws IOException
     */
    OutputStream getEncodedImageOutput() throws IOException;

}
