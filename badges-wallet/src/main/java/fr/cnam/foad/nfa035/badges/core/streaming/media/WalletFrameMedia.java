package fr.cnam.foad.nfa035.badges.core.streaming.media;

import java.io.RandomAccessFile;

/**
 * Interface spécifiant le contrat comportemental du media de sérialisation
 *
 * Cette interface a un rôle de spécialisation de son interface parente,
 * dans le sens où elle doit permettre de maintenir l'état du flux d'entrée afin de favoriser la reprise.
 *
 * De plus, cette interface permet de conserver l'état du média, afin de facilité la gestion des métadonnées
 * et ainsi favoriser la reprise au niveau des flux d'écriture.
 *
 *
 */
public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {

    /**
     * Retourne le nombre de ligne (ou enregistrements) du media
     * @return int
     */
    long getNumberOfLines();

    /**
     * Incrémente le nombre de lignes
     */
    void incrementLines();


}
