package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.core.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.AbstractStreamingImageDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.setSourceOutputStream(sourceOutputStream);
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return le flux de lecture
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }


}
