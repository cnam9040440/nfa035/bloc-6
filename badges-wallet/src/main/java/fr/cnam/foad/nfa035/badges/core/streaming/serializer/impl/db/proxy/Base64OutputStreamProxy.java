package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.proxy;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.output.CloseShieldOutputStream;

import java.io.Closeable;
import java.io.FilterOutputStream;
import java.io.Flushable;
import java.io.IOException;

public class Base64OutputStreamProxy extends FilterOutputStream implements Closeable, Flushable, AutoCloseable {

    Base64OutputStream bos = null;
    CloseShieldOutputStream wrapper = null;

    public Base64OutputStreamProxy(Base64OutputStream proxy) {
        super(proxy);
        this.bos = proxy;
        this.wrapper = CloseShieldOutputStream.wrap(proxy);
    }

    @Override
    public void close() throws IOException {
        wrapper.close();
    }

    @Override
    public void flush() throws IOException {
        bos.eof();
        bos.flush();
    }

    @Override
    public void write(byte[] array, int offset, int len) throws IOException {
        wrapper.write(array, offset, len);
    }

    @Override
    public void write(int i) throws IOException {
        bos.write(i);
    }

    @Override
    public void write(byte[] b) throws IOException {
        bos.write(b);
    }

}
