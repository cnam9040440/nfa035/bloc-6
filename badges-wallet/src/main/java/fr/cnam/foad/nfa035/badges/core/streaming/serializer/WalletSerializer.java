package fr.cnam.foad.nfa035.badges.core.streaming.serializer;

import java.io.IOException;

/**
 * Interface définissant le comportement attendu d'un objet servant à la sérialisation d'un badge dans un wallet
 * La particularité de cet objet selon le contrat attendu serait de permettre l'annulation (la suppression) d'un badge
 * Le reste des opération étant déjà hérité de la super interface.
 *
 * @param <S>
 * @param <M>
 */
public interface WalletSerializer<S, M> extends ImageStreamingSerializer<S,M> {

    /**
     *
     * @param source
     * @param media
     * @throws IOException
     */
    void rollback(S source, M media) throws IOException;

}
