package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

    private static final Logger LOG = LogManager.getLogger(WalletDeserializerDirectAccessImpl.class);

    private OutputStream sourceOutputStream;
    Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     * @param metas les métadonnées du wallet, si besoin
     */
    public WalletDeserializerDirectAccessImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) {
        this.setSourceOutputStream(sourceOutputStream);
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);
        // Lecture de la ligne et parsage des différents champs contenus dans la ligne, de 2 façons:
        // 1/ FACILE: String[] data = media.getChannel().readLine().split(";");
        // 2/ Optimisé par le fait de passer par un BufferedReader,
        // mais Attention aux évolutions plus pertinentes à venir, éventuellement:
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
           getDeserializingStream(data[6]).transferTo(os);
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String serial = data[3];
        Date end = null;
        try {
            targetBadge.setBegin(format.parse(data[4]));
        } catch (ParseException e) {
            LOG.error("Problème de parsage, on considère la date Nulle", e);
        }
        try {
            end = format.parse(data[5]);
        } catch (ParseException e) {
            LOG.error("Problème de parsage, on considère la date Nulle", e);
        }
        if(!targetBadge.getSerial().equals(serial) || !targetBadge.getEnd().equals(end)){
            LOG.error("Badge Incohérent!");
            throw new IOException("Badge Incohérent!!");
        }
    }

    /**
     * {@inheritDoc}
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


    /**
     * {@inheritDoc}
     *
     * Inutile => Non pris encharge par cet Objet
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }

    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
