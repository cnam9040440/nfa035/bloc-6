package fr.cnam.foad.nfa035.badges.core.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.core.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.media.ResumableImageFrameMedia;

import java.io.*;

/**
 * Implémentation d'ImageFrame pour un simple Fichier texte comme canal,
 * permettant de reprendre la lecture du badge suivant (de la ligne suivante) après chaque badge ingéré.
 */
public class ResumableImageFileFrame extends AbstractImageFrameMedia<File> implements ResumableImageFrameMedia<File> {

    private BufferedReader reader;

    private FileOutputStream fileOutputStream = null;

    public ResumableImageFileFrame(File walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        if (fileOutputStream == null){
            this.fileOutputStream = new FileOutputStream(getChannel(), true);
        }
        return fileOutputStream;
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new FileInputStream(getChannel());
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (reader == null){
            this.reader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel())));
        }
        return this.reader;
    }

}
