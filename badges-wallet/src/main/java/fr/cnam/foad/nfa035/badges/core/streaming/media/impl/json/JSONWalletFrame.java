package fr.cnam.foad.nfa035.badges.core.streaming.media.impl.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.core.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Implémentation d'ImageFrame pour un Fichier JSON comme canal
 * Permet notamment de gérer l'amorce du csv afin de le rendre exploitable par accès direct (RandomAccessFile)
 */
public class JSONWalletFrame extends WalletFrame {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrame.class);

    private FileOutputStream encodedImageOutput = null;

    /**
     * Constructeur élémentaire
     *
     * @param walletDatabase
     */
    public JSONWalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     *
     * Permet l'initialisation du fichier csv (amorce) si celui-ci est vierge,
     * sinon récupère l'amorce et l'utilise pour initialiser les méta-données
     * globales du média (nombre de lignes...)
     *
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {

        if (encodedImageOutput == null){

            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            PrintWriter writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);

            long fileLength = file.length();
            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
            ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
            DigitalBadge amorce;

            if(fileLength == 0) {
                // Ecriture de l'amorce
                amorce = new DigitalBadge(null,null,null, new DigitalBadgeMetadata(1,0,-1),null);
                writer.printf("[{\"badge\":");
                objectMapper.writeValue(encodedImageOutput,amorce);
                writer.printf("},{\"payload\":null}]");
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                LOG.debug("Dernière ligne du fichier : {}", lastLine);
                String badgeAmorce = lastLine.split(",\\{\"payload")[0].split(".*badge\":")[1];
                LOG.debug("Badge amorce : {}", badgeAmorce);
                amorce = objectMapper.readValue(badgeAmorce,DigitalBadge.class);
                this.setNumberOfLines(amorce.getMetadata().getBadgeId() - 1);
                file.seek(fileLength);
            }

        }
        return encodedImageOutput;
    }

}
