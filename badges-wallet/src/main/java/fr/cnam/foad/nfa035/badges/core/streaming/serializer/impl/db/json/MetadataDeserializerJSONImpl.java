package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class MetadataDeserializerJSONImpl extends MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerJSONImpl.class);

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return Set<DigitalBadge>
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        BufferedReader br = media.getEncodedImageReader(false);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);


        return br.lines()
                .map(
                    l-> {
                        try {
                            String badge = l.split(",\\{\"payload")[0].split(".*badge\":")[1];
                            DigitalBadge digitalBadge = objectMapper.readValue(badge, DigitalBadge.class);
                            digitalBadge.setSerializeHash(true);
                            return digitalBadge.getMetadata().getImageSize() == -1 ? null : digitalBadge;
                        } catch (IOException ioException) {
                            LOG.warn("Problème de parsage JSON, on considère l'enregistrement vide => supprimé, donc à défragmenter");
                            return markAsDeleted(l);
                        }
                        catch (Exception e){
                            return markAsDeleted(l);
                        }
                    }
                ).filter(x -> x!=null).collect(Collectors.toSet());
    }

    /**
     * {@inheritDoc}
     *
     * @param line
     * @return l'instance de badge vide/supprimé
     */
    @Override
    public DigitalBadge markAsDeleted(String line){
        return DigitalBadge.makeDeadBadge(Long.parseLong(line.split(";")[1]));
    }


}
