package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.SortedMap;

/**
 * Interface définissant le comportement SUPPLEMENTAIRE d'un DAO destinée à la gestion de badges digitaux
 * PAR ACCES DIRECT, et en JSON
 */
public interface JSONBadgeWalletDAO extends DirectAccessBadgeWalletDAO {

    void addBadge(DigitalBadge badge, InputStream source) throws IOException;

    /**
     * Facilité pour obtenir un badge à partir de ses métadonnées strictement intrinsèques au Badge
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;

    /**
     * Facilité pour obtenir une Map de métadonnées triée au lieu d'un Set (trié)
     *
     * @return SortedMap<DigitalBadge, DigitalBadgeMetadata>
     * @throws IOException
     */
    SortedMap<DigitalBadge, DigitalBadgeMetadata> getWalletMetadataMap() throws IOException;


    /**
     * Facilité pour obtenir une Map de métadonnées triée par ID de badge
     *
     * @return SortedMap<Long, DigitalBadge>
     * @throws IOException
     */
    SortedMap<String, DigitalBadge> getWalletMapByHash() throws IOException;
}
