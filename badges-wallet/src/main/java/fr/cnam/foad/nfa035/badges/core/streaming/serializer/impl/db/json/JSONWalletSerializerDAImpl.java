package fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.AbstractWalletSerializer;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class JSONWalletSerializerDAImpl extends AbstractWalletSerializer {

    /**
     * The Metas.
     */
    DigitalWallet metas;


    InputStream sourceInputStream = null;

    /**
     * Gets metas.
     *
     * @return the metas
     */
    @Override
    public DigitalWallet getMetas() {
        return metas;
    }

    /**
     * Instantiates a new Json wallet serializer da.
     *
     * @param metas the metas
     */
    public JSONWalletSerializerDAImpl(DigitalWallet metas) {
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return sourceInputStream == null ? new FileInputStream(source.getBadge()) : sourceInputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(WalletFrameMedia media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(),true,0,null));
    }


    /**
     * {@inheritDoc}
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public final void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException {
        if (metas.getAllBadges().contains(source)){
            throw new IOException("Badge déjà présent dans le Wallet");
        }

        // Attention, on ne veut pas sérialiser le hash
        source.setSerializeHash(false);
        // en entrée de sérialisation on doit avoir les métadonnées initialisées si appel distant par api. SInon on récupère les infos du fichier local
        long fileSize = source.getMetadata() == null ? Files.size(source.getBadge().toPath()):source.getMetadata().getImageSize();

        OutputStream os = media.getEncodedImageOutput();

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        RandomAccessFile directFile = media.getChannel();
        String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(directFile);
        DigitalBadge amorce = objectMapper.readValue(lastLine.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);
        DigitalBadgeMetadata meta = new DigitalBadgeMetadata(amorce.getMetadata().getBadgeId(), amorce.getMetadata().getWalletPosition(), fileSize);
        source.setMetadata(meta);
        // Gestion du mimeType
        String fileName = source.getBadge().getName();
        ManagedImages mimeType = ManagedImages.valueOf(fileName.substring(fileName.lastIndexOf('.') + 1));
        meta.setImageType(mimeType.getMimeType());
        directFile.seek(meta.getWalletPosition());

        PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
        if (meta.getBadgeId() == 1){
            writer.printf("[");
        }
        writer.printf("[{\"badge\":");
        objectMapper.writeValue(os,source);
        writer.printf("},{\"payload\":\"");

        try(OutputStream eos = getSerializingStream(media)) {
            getSourceInputStream(source).transferTo(eos);
            eos.flush();
        }

        writer.printf("\"}],\n");

        long newPosition = directFile.getFilePointer();


        DigitalBadge nouvelleAmorce = new DigitalBadge(null,null,null, new DigitalBadgeMetadata(meta.getBadgeId() + 1 ,newPosition,-1),null);
        writer.printf("[{\"badge\":");
        objectMapper.writeValue(os,nouvelleAmorce);
        writer.printf("},{\"payload\":null}]]");

        media.incrementLines();
    }


    @Override
    public <K extends InputStream> void changeSourceInputStream(K inputStream) throws IOException {
        this.sourceInputStream = inputStream;
    }
}
