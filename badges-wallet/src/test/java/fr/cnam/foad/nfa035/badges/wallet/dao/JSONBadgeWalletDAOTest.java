package fr.cnam.foad.nfa035.badges.wallet.dao;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json.ManagedImages;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class JSONBadgeWalletDAOTest {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabaseException() {

        try {

            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");

            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge);

            assertThrows(IOException.class, () -> {
                dao.addBadge(badge);
            });

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabase(){

        try {

            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA034", begin, end, null, image2);
            dao.addBadge(badge2);

            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);

            try (BufferedReader reader = new BufferedReader(new FileReader(walletDatabase))) {

                JsonFactory jsonFactory = new JsonFactory();
                jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
                ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

                String serializedImage = reader.readLine();
                LOG.info("1ère ligne:\n{}", serializedImage);
                DigitalBadge serializedBadge1 = objectMapper.readValue(serializedImage.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

                String serializedImage2 = reader.readLine();
                LOG.info("2ème ligne:\n{}", serializedImage2);
                DigitalBadge serializedBadge2 = objectMapper.readValue(serializedImage2.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

                String serializedImage3 = reader.readLine();
                LOG.info("3ème ligne:\n{}", serializedImage3);
                DigitalBadge serializedBadge3 = objectMapper.readValue(serializedImage3.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);


                // Assertions
                assertEquals(badge1, serializedBadge1);
                String mime1 = serializedBadge1.getMetadata().getImageType();
                assertEquals(ManagedImages.valueOf(mime1.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());
                assertEquals(badge2, serializedBadge2);
                String mime2 = serializedBadge2.getMetadata().getImageType();
                assertEquals(ManagedImages.valueOf(mime2.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());
                assertEquals(badge3, serializedBadge3);
                String mime3 = serializedBadge3.getMetadata().getImageType();
                assertEquals(ManagedImages.valueOf(mime3.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.jpg.getMimeType());

            }

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }


    /**
     * Teste la récupération des Métadonnées
     */
    @Test
    void testGetMetadata() throws IOException {
        DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");
        Set<DigitalBadge> metaSet = dao.getWalletMetadata().getAllBadges();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

        assertEquals(3, metaSet.size());
        Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
        DigitalBadge b1 = it.next();
        assertEquals(new DigitalBadgeMetadata(1, 0,557), b1.getMetadata());
        DigitalBadge b2 = it.next();
        assertEquals(new DigitalBadgeMetadata(2,919, 906), b2.getMetadata());
        DigitalBadge b3 = it.next();
        assertEquals(new DigitalBadgeMetadata(3,2303, 35664), b3.getMetadata());

        String mime1 = b1.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime1.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());
        String mime2 = b2.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime2.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());
        String mime3 = b3.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime3.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.jpg.getMimeType());

    }


    /**
     * Teste la récupération des Métadonnées avec des enregistrements supprimés
     */
    @Test
    void testGetMetadataWithDeletedRecords() throws IOException {
       doTestGetMetadataWithDeletedRecords("db_wallet_delete.json");
    }

    /**
     * Documentez-moi
     * @param fileName
     * @throws IOException
     */
    private void doTestGetMetadataWithDeletedRecords(String fileName) throws IOException{
        DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + fileName);
        DigitalWallet wallet = dao.getWalletMetadata();
        Set<DigitalBadge> metaSet = wallet.getAllBadges();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

        assertEquals(2, metaSet.size());

        Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
        DigitalBadge b1 = it.next();
        assertEquals(new DigitalBadgeMetadata(1, 0,557), b1.getMetadata());
        DigitalBadge b2 = it.next();
        assertEquals(new DigitalBadgeMetadata(2,919, 906), b2.getMetadata());

        assertThrows(NoSuchElementException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                it.next();
            }
        });

        assertEquals(0, wallet.getDeletingBadges().size());
        assertEquals(1, wallet.getDeadLinesPositions().size());
        assertEquals(2303, wallet.getDeadLinesPositions().iterator().next());

        String mime1 = b1.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime1.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());
        String mime2 = b2.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime2.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());

    }


    /**
     * Teste la récupération des Métadonnées
     */
    @Test
    void testGetMetadataMap() throws IOException {
        JSONBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");
        Map<DigitalBadge, DigitalBadgeMetadata> metaMap = dao.getWalletMetadataMap();
        LOG.info("Et voici les Métadonnées sous forme de Map triée: {}", dao.getWalletMetadata());

        assertEquals(3, metaMap.size());
        Iterator<DigitalBadge> it = metaMap.keySet().iterator();
        assertEquals(new DigitalBadgeMetadata(1, 0,557), metaMap.get(it.next()));
        assertEquals(new DigitalBadgeMetadata(2,919, 906), metaMap.get(it.next()));
        assertEquals(new DigitalBadgeMetadata(3,2303, 35664), metaMap.get(it.next()));
    }

    /**
     * Test la récupération séquentielle des badges du wallet
     */
    @Test
    public void testGetBadgeFromDatabaseByMetadata(){
        try {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("NFA033", begin, end, new DigitalBadgeMetadata(1, 0,557), null);
            DigitalBadge expectedBadge2 = new DigitalBadge("NFA034", begin, end, new DigitalBadgeMetadata(2, 919,906), null);
            DigitalBadge expectedBadge3 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(3, 2303,35664), null);
            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            dao.getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            dao.getBadgeFromMetadata(fileBadgeStream2, expectedBadge2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            dao.getBadgeFromMetadata(fileBadgeStream3, expectedBadge3);

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

    /**
     * Test la récupération séquentielle des badges du wallet
     */
    @Test
    public void testGetBadgeFromDatabaseByStrictMetadata(){
        try {
            JSONBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage = new File(RESOURCES_PATH + "petite_image_2.png");
            File extractedImage = new File(RESOURCES_PATH + "petite_image_2_ext.png");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadgeMetadata badgeMetas = new DigitalBadgeMetadata(2, 919,906);

            OutputStream fileBadgeStream = new FileOutputStream(extractedImage);
            dao.getBadgeFromMetadata(fileBadgeStream, badgeMetas);

            assertArrayEquals(Files.readAllBytes(originImage.toPath()), Files.readAllBytes(extractedImage.toPath()));
            LOG.info("Badge 2 récupéré avec succès");


        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }


    /**
     * Test la suppression d'un badge du Wallet
     */
    @Test
    public void testDeleteBadgeFromDatabase(){
        try {

            JSONBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2022-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);
            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA034", begin, end, null, image2);
            dao.addBadge(badge2);
            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);

            dao.removeBadge(badge3);

            doTestGetMetadataWithDeletedRecords("wallet.json");

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    /**
     * Test la récupération de badge avec tentative de Fraude
     */
    @Test
    public void testGetBadgeFromDatabaseByMetadataWithFraud(){
        try {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("SUPER_STAR", begin, end, new DigitalBadgeMetadata(1, 0,557), null);

            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            assertThrows(IOException.class, () -> {
                dao.getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            });

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

}
