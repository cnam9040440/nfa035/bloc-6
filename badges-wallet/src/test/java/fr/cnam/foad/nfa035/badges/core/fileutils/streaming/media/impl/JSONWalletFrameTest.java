package fr.cnam.foad.nfa035.badges.core.streaming.media.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.core.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class JSONWalletFrameTest {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrameTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste l'initialisation du Wallet
     */
    @Test
    public void testGetEncodedImageOutputWhenDatabaseIsEmpty() throws IOException {


        try(RandomAccessFile directFile = new RandomAccessFile(walletDatabase, "rw");
            BufferedReader reader = new BufferedReader(new FileReader(walletDatabase)))
        {
            JSONWalletFrame wallet = new JSONWalletFrame(directFile);
            wallet.getEncodedImageOutput();

            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
            ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

            String targetAmorceJson = "[{\"badge\":{\"metadata\":{\"badgeId\":1,\"walletPosition\":0,\"imageSize\":-1}}},{\"payload\":null}]";
            DigitalBadge targetAmorce = objectMapper.readValue(targetAmorceJson.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

            String actualAmorceJson = reader.readLine();
            DigitalBadge actualAmorce = objectMapper.readValue(actualAmorceJson.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

            assertEquals(targetAmorce, actualAmorce);
        }

    }

    /**
     * Teste l'initialisation du Wallet
     */
    @Test
    public void testGetEncodedImageOutput() throws IOException {

        try(RandomAccessFile directFile = new RandomAccessFile(RESOURCES_PATH+ "db_wallet.json", "rw")){
            JSONWalletFrame wallet = new JSONWalletFrame(directFile);
            wallet.getEncodedImageOutput();
            assertEquals(3, wallet.getNumberOfLines());
        }
    }

}
