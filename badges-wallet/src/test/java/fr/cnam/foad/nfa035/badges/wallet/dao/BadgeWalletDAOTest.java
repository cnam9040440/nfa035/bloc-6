package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.core.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.core.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.SingleBadgeWalletDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class BadgeWalletDAOTest {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabase(){

        try {

            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");

            // 1er Badge

            File image = new File(RESOURCES_PATH + "petite_image.png");
            dao.addBadge(image);

            BufferedReader reader = new BufferedReader(new FileReader(walletDatabase));
            String serializedImage = reader.readLine();
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;
            String[] data = serializedImage.split(";");

            assertEquals(1, Integer.parseInt(data[0]));
            assertEquals(Files.size(image.toPath()), Long.parseLong(data[1]));
            assertEquals(encodedImage, data[2]);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            dao.addBadge(image2);
            String serializedImage2 = reader.readLine();
            String encodedImage2 = (String) serializer.serialize(image2);
            serializedImage2 = serializedImage2.replaceAll("\n","").replaceAll("\r","") ;
            String[] data2 = serializedImage2.split(";");

            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            dao.addBadge(image3);
            String serializedImage3 = reader.readLine();
            String encodedImage3 = (String) serializer.serialize(image3);
            serializedImage3 = serializedImage3.replaceAll("\n","").replaceAll("\r","") ;
            String[] data3 = serializedImage3.split(";");

            assertEquals(2, Integer.parseInt(data2[0]));
            assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[1]));
            assertEquals(encodedImage2, data2[2]);

            assertEquals(3, Integer.parseInt(data3[0]));
            assertEquals(Files.size(image3.toPath()), Long.parseLong(data3[1]));
            assertEquals(encodedImage3, data3[2]);

            reader.close();

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    /**
     * Test la récupération séquentielle des badges depuis un wallet multi-badge
     */
    @Test
    public void testGetBadgeFromDatabase(){
        try {
            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.csv");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");

            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            dao.getBadge(fileBadgeStream1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            dao.getBadge(fileBadgeStream2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            dao.getBadge(fileBadgeStream3);

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

    /**
     * Teste l'ajout d'un badge
     */
    @Test
    public void testAddBadge(){
        try {

            File image = new File(RESOURCES_PATH + "petite_image.png");
            SingleBadgeWalletDAOImpl dao = new SingleBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
            dao.addBadge(image);

            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

            assertEquals(serializedImage, encodedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    /**
     * Test la récupération d'un badge
     */
    @Test
    public void testGetBadge(){
        try {
            SingleBadgeWalletDAOImpl dao = new SingleBadgeWalletDAOImpl(RESOURCES_PATH + "wallet_full.csv");
            File extractedImage = new File(RESOURCES_PATH + "petite_image_extraite.png");

            ByteArrayOutputStream memoryBadgeStream = new ByteArrayOutputStream();
            dao.getBadge(memoryBadgeStream);
            byte[] deserializedImage = memoryBadgeStream.toByteArray();

            // Vérification 1
            byte [] originImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image.png").toPath());
            //assertEquals(new String(originImage, StandardCharsets.UTF_8), new String(deserializedImage, StandardCharsets.UTF_8));
            assertArrayEquals(originImage, deserializedImage);

            // Vérification 2
            //dao = new SingleBadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
            OutputStream fileBadgeStream = new FileOutputStream(extractedImage);
            dao.getBadge(fileBadgeStream);


            deserializedImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image_extraite.png").toPath());
            assertArrayEquals(originImage, deserializedImage);
            LOG.info("Badge récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

}
