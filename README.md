# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

# Applications WEB, MVC, Services Rest
 ++ Mise en place d'une API Rest
 

## Contexte
* Au programme de ce cours: Services Rest avancés

Comme à son habitude, notre meilleur client revient nous voir avec un fort enthousiasme.
Il a eu vent des projets de refonte JSON, et aussi de l'éventuelle création de services Rest. IL n'a vraiment pas tout compris, mais il commence à répandre la nouvelle que notre architecture est déjà 100% Restful
EN plus de ça, il nous demande de prototyper rapidement une interface Web s'appuyant sur nos services...mais donc, il faut les développer de toute urgence !

## Objectifs
* Mises en application :
- [x] (Exercice 1) Service REST : Lecture d'un Badge
- [x] **(Exercice 2) Service REST : suppression d'un Badge**
- [x] (Exercice 3) Service REST : Ajout d'un Badge
- [x] (Exercice 4) "Défragmentation" : Job de nettoyage des lignes supprimées par reconstitution d'une base propre

----

- [ ] Développer un traitement "batch" permettant de nettoyer le fichier de base de données Json suite à un certain nombre de suppressions
  - [ ] Il s'agit de lire le fichier de base json octet par octet en sautant les lignes supprimées (comme nous en connaissons les positions), et d'écrire cela dans la foulée vers un nouveau fichier, temporaire
  - [ ] En post-traitement il conviendra d'archiver la base originale, puis de renommer le fichier temporaire du nom du fichier original


Afin d'implémenter le service de suppression de badge, abordons le problème par impacts, couche par couche.

## Impacts

### Impact sur le Modèle

### Impact sur la désérialisation des Métadonnées

### Impact sur la sérialisation
      
### Impact sur Les DAOs
     
### Impact sur la couche de Service

## Tests

### Unitaires

### D'acceptance (de recette)

  
**FIN**

----

## Ressources supplémentaires







