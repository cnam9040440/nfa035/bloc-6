package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.WalletDBFormats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Commentez-moi
 */
@Component("walletDAOFactory")
@Order(value = 1)
public class WalletDAOFactory {

    @Qualifier("jsonBadge")
    @Autowired
    DirectAccessBadgeWalletDAO jsonBadgeDao;


    @Qualifier("directAccess")
    @Autowired
    DirectAccessBadgeWalletDAO directAccessDao;

    /**
     * Commentez-moi
     * @return BadgePanel
     */
    @Bean
    @Qualifier("guiSelected")
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DirectAccessBadgeWalletDAO createInstance(@Value("${db.format?:jsonBadge}") String dbFormat) {
        switch (WalletDBFormats.valueOf(dbFormat)){
            case jsonBadge:
                return jsonBadgeDao;
            case directAccess:
                return directAccessDao;
            case simple:
                throw new RuntimeException("Format non supporté");
            default:
                return jsonBadgeDao;
        }
    }

}