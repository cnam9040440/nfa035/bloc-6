package fr.cnam.foad.nfa035.badges.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

/**
 * Trouvé par exemple sur https://www.it-swarm-fr.com/fr/spring/requestpart-avec-demande-mixte-en-plusieurs-parties-spring-mvc-3.2/1071864475/
 * Ce composant permet à Spring de mieux gérer les services Rest avec des entrées mixtes (json + fichier binaire par exemple)
 *
 */
@Component
public class MultipartJackson2HttpMessageConverter extends AbstractJackson2HttpMessageConverter {

    /**
     * Convertisseur pour supporter les requêtes HTTP avec un Header "Content-Type: multipart/form-data"
     */
    public MultipartJackson2HttpMessageConverter(ObjectMapper objectMapper) {
        super(objectMapper, MediaType.APPLICATION_OCTET_STREAM);
    }

    /**
     * {@inheritDoc}
     *
     * @param clazz
     * @param mediaType
     * @return
     */
    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return false;
    }

    /**
     * {@inheritDoc}
     *
     * @param type
     * @param clazz
     * @param mediaType
     * @return
     */
    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        return false;
    }

    /**
     * {@inheritDoc}
     *
     * @param mediaType
     * @return
     */
    @Override
    protected boolean canWrite(MediaType mediaType) {
        return false;
    }
}