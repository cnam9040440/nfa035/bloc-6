package fr.cnam.foad.nfa035.badges.engine.processing;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;

@Component
public class BadgeItemProcessor implements ItemProcessor<DigitalBadge, Map.Entry<DigitalBadge, byte[]>> {

    @Autowired
    JSONBadgeWalletDAO dao;

    /**
     * Commentez-moi
     * @param digitalBadge
     * @return
     * @throws Exception
     */
    @Override
    public Map.Entry<DigitalBadge, byte[]> process(DigitalBadge digitalBadge) throws Exception {
        ByteArrayOutputStream imageItemStream = new ByteArrayOutputStream();
        dao.getBadgeFromMetadata(imageItemStream, digitalBadge);
        digitalBadge.setBadge(new File(digitalBadge.getMetadata().getImageType()));
        return Map.entry(digitalBadge,imageItemStream.toByteArray());
    }
}