package fr.cnam.foad.nfa035.badges.engine;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

@ComponentScan("fr.cnam.foad.nfa035.badges")
@EnableBatchProcessing
public class BadgesDefragmentationConfiguration{

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    /**
     * Documentez-moi
     * @param step1
     * @param step2
     * @return
     */
    @Bean
    public Job badgesDefragJob(@Qualifier("step1") Step step1, @Qualifier("step2") Step step2) {
        return jobs.get("defragJob").start(step1)
                .next(step2)
        .build();
    }

    /**
     * Documentez-moi
     * @param reader
     * @param processor
     * @param writer
     * @return
     */
    @Bean
    protected Step step1(ItemReader<DigitalBadge> reader,
                         ItemProcessor<DigitalBadge, Map.Entry<DigitalBadge, byte[]>> processor,
                         ItemWriter<Map.Entry<DigitalBadge, byte[]>> writer) {
        return steps.get("step1")
                .<DigitalBadge, Map.Entry<DigitalBadge, byte[]>> chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    /**
     * Documentez-moi
     * @param tasklet
     * @return
     */
    @Bean
    protected Step step2(Tasklet tasklet) {
        return steps.get("step2")
                .tasklet(tasklet)
                .build();
    }

    /**
     * Documentez-moi
     * @return
     */
    @Bean
    public BatchConfigurer batchConfigurer() {
        return new DefaultBatchConfigurer() {
            @Override
            protected JobRepository createJobRepository() throws Exception {
                MapJobRepositoryFactoryBean factory = new MapJobRepositoryFactoryBean();
                //factory.setTransactionManager(transactionManager);
                return factory.getObject();
            }
        };
    }

    /**
     * Documentez-moi
     * @param dao
     * @return
     * @throws IOException
     */
    @Bean
    @Scope("singleton")
    public DigitalWallet wallet(JSONBadgeWalletDAO dao) throws IOException {
        this.wallet = dao.getWalletMetadata();
        return wallet;
    }

    DigitalWallet wallet;

    /**
     * Documentez-moi
     * @return
     */
    @DependsOn("wallet")
    @Bean
    public List<DigitalBadge> items() {
        Set<DigitalBadge> badges = wallet.getAllBadges();
        if (wallet.getDeadLinesPositions().size() >= badges.size()/3)
        {
            return new ArrayList<>(badges);
        }
        else return new ArrayList<>();
    }

    @Bean
    @Profile("test")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");

        return dataSource;
    }
}