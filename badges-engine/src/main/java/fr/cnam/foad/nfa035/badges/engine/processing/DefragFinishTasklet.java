package fr.cnam.foad.nfa035.badges.engine.processing;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Commentez-moi
 */
@Component
public class DefragFinishTasklet implements Tasklet, InitializingBean {

    @Value("${db.path:badges-wallet/src/test/resources/db_wallet.json}")
    private String dbPath;

    @Value("${defrag.job.dir:badges-wallet/src/test/resources/}")
    private String workDir;

    /**
     * COmmentez-moi
     * @param contribution
     * @param chunkContext
     * @return
     * @throws Exception
     */
    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {
        Path db = Paths.get(dbPath);
        Path defrag = Paths.get(workDir + "replicate.json");
        // Aucun réplicat => pas de défragmentation");
        if (!defrag.toFile().isFile()){
            return RepeatStatus.FINISHED;
        }
        Assert.state(db.toFile().isFile(),"La base sélectionnée n'est pas un fichier");
        Files.delete(db);
        Files.move(defrag,db);
        Assert.state(db.toFile().isFile(),"La base n'a pu être défragmentée correctement...");

        return RepeatStatus.FINISHED;
    }

    /**
     * Commentez-moi
     * @throws Exception
     */
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(dbPath, "dbPath must be set");
    }
}