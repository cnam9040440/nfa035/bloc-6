package fr.cnam.foad.nfa035.badges.engine.processing;

import fr.cnam.foad.nfa035.badges.core.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.core.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.core.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * Commentez-moi
 */
@Component
public class BadgeItemWriter implements ItemWriter<Map.Entry<DigitalBadge, byte[]>> {

    private String workDir;

    /**
     * Constructeur
     * @param workDir
     */
    public BadgeItemWriter(@Value("${defrag.job.dir:badges-wallet/src/test/resources/}") String workDir) {
        this.workDir = workDir;
    }

    /**
     * Commentez-moi
     * @param items
     * @throws Exception
     */
    @Override
    public void write(List<? extends Map.Entry<DigitalBadge, byte[]>> items) throws Exception {
        System.out.println(MessageFormat.format("Ecriture des items {0}", items));
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(new File(workDir + "replicate.json"), "rw"))) {

            DigitalWallet defragWallet = JSONBadgeWalletDAOImpl.getWalletMetadata("rw", media);
            ImageStreamingSerializer<DigitalBadge, WalletFrameMedia> serializer = new JSONWalletSerializerDAImpl(defragWallet);

            for (Map.Entry<DigitalBadge, byte[]> badgeEntry : items){
                serializer.changeSourceInputStream(new ByteArrayInputStream(badgeEntry.getValue()));
                DigitalBadge badge = badgeEntry.getKey();
                serializer.serialize(badgeEntry.getKey(), media);
            }
        }
    }
}