package fr.cnam.foad.nfa035.badges.engine.processing;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class BadgeItemReader implements ItemReader<DigitalBadge> {

    List<DigitalBadge> items;

    public BadgeItemReader(@Qualifier("items") List<DigitalBadge> items) throws IOException {
        this.items = items;
    }

    /**
     * Commentez-moi
     * @return DigitalBadge les metadonnées d'un badge lu
     * @throws Exception
     * @throws UnexpectedInputException
     * @throws NonTransientResourceException
     * @throws ParseException
     */
    public DigitalBadge read() throws Exception, UnexpectedInputException,
            NonTransientResourceException, ParseException {

        if (!items.isEmpty()) {
            return items.remove(0);
        }
        return null;
    }

}