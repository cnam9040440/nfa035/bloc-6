package fr.cnam.foad.nfa035.badges.engine;


import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.test.AssertFile;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@TestPropertySource(properties = {"spring.datasource.initialize=false","spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration"})
@SpringBatchTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { BadgesDefragmentationConfiguration.class })
@ActiveProfiles("test")
public class BatchDefragJobIntegrationTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private JobRepositoryTestUtils jobRepositoryTestUtils;

    /**
     * Rétablit l'état des jeux de données
     * @throws IOException
     */
    @After
    public void cleanUp() throws IOException {

        jobRepositoryTestUtils.removeJobExecutions();
        Path backup = Paths.get("src/test/resources/db_wallet_dirty.json.bak");
        Path db = Paths.get("src/test/resources/db_wallet_dirty.json");
        Files.delete(db);
        Files.copy(backup,db);
    }

    /**
     * Permet de régler les jeux de données
     */
    @BeforeClass
    public static void setProperties(){
        System.setProperty("db.path", "src/test/resources/db_wallet_dirty.json");
        System.setProperty("defrag.job.dir", "src/test/resources/");

    }

    /**
     * Documentez-moi
     *
     * @throws Exception
     */
    @Test
    public void givenReferenceOutput_whenJobExecuted_thenSuccess() throws Exception {

        // given
        FileSystemResource expectedResult = new FileSystemResource("src/test/resources/db_wallet_defrag.json");
        FileSystemResource actualResult = new FileSystemResource("src/test/resources/db_wallet_dirty.json");

        // when
        JobExecution jobExecution = jobLauncherTestUtils.launchJob(defaultJobParameters());
        JobInstance actualJobInstance = jobExecution.getJobInstance();
        ExitStatus actualJobExitStatus = jobExecution.getExitStatus();

        // then
        AssertFile.assertFileEquals(expectedResult, actualResult);
        assertEquals("defragJob", actualJobInstance.getJobName());
        assertEquals("COMPLETED", actualJobExitStatus.getExitCode());
    }

    /**
     * Documentez-moi
     * @return
     */
    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        paramsBuilder.addString("mon.param.1", "valeur1");
        paramsBuilder.addString("mon.param.2", "valeur2");
        return paramsBuilder.toJobParameters();
    }
}